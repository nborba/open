"""
"""
import sys

std = sys.stdout
f   = open('filename.txt', 'a')

#Escreve no arquivo mas não printa na tela
"""sys.stdout = f

print("Pass 1")
print("Pass 2")
print("Pass 3")"""

#Escreve no arquivo mas não printa na tela
"""f.write("Teste OK")
f.write("Teste OK 2")"""

#Escreve no arquivo mas não printa na tela
"""print("Pass 4", file=f)
print("Pass 5", file=f)
print("Pass 6", file=f)"""

#Printa na tela e escreve no arquivo
"""print("Pass 7")
f.write("Pass 7")

print("Pass 8")
f.write("Pass 8")

print("Pass 9")
f.write("Pass 9")"""

def parouimpar(numero):
    if numero % 2 > 0:
        return (print("Teste 1: PASS"),
                f.write("\nTeste 1, Pass"))
    else:
        return print("Par")

N = float(input("Digite um numero: "))
parouimpar(N)
