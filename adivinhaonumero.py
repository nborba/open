from random import randint

#Veja meu código rodando em: https://repl.it/@nborba/adivinhaonumero

#Variavel declararada, não precisei dizer o tipo.
#randint significa random integer ou em PT-BR "inteiro aleatório"
#essa única linha guarda na minha váriavel um número aleatórido de 0 a 10

segredo = randint(0,10)

#Aqui iniciamos um loop infinito, while = enquanto
while True: 
  
  #Aqui pedimos pro usuário digitar e salvo na variavel em uma única linha
  resposta = input("Digite o número você acha que é: ")
  
  #Comparo o que foi digitado com o número que precisa ser descoberto
  #se estiver certo, parabenizo, se estiver errado o loop se repete até acertar
  if resposta == str(segredo):
    print("Parabéns, você acertou!\n O número secreto é: %i" %segredo)
    break
  else:
    pass
  
#não precisamos indicar o fim do loop escrevendo algo, basta não ter nada após ele
#ou caso haja continuação, esta deve estar da identação, ou seja, deve estar na mesma
#coluna no while, praticamente encostado no canto, não deve haver espaços antes.
