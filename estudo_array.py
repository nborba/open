"""
Estudo sobre iteração em dicionários e retorno de seus valores.
"""

ERRO = {"CASE1": ["0x0001","0x0002"],
        "CASE2": ["0x0003","0x0004"]}

DICI = {"CASE1": {"PAR": "0x0001", "RES": "0x0002"},
        "CASE2": {"PAR": "0x0003", "RES": "0x0004"}}

#USANDO DICIONARIO + LISTA    
for x in ERRO:
    print("PARAMETRO: "+ERRO[x][0])
    print("RESULTADO: "+ERRO[x][1]+"\n")

print("#################\n")

#USANDO DICIONARIO + DICIONARIO
for x in DICI:
    print("PARAMETRO: "+DICI[x]["PAR"])
    print("RESULTADO: "+DICI[x]["RES"]+"\n")